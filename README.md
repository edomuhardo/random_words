# Random Words Game

Make sure you have installed ruby on your system.


##Gem Installation
```
run `gem install rspec`
```

##To run the program
```
run './random_words' on Terminal
```

##To run all the tests
```
run 'rspec'
```
