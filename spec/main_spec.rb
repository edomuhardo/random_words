require 'main'

describe Main do
  subject { Main.new(data) }
  let(:data) { ['testing-1', 'testing-2'] }

  describe 'initialize' do
    it "assigns @words = ['testing-1', 'testing-2']" do
      expect(subject.instance_variable_get(:@words)).to eq(['testing-1', 'testing-2'])
    end
    it "assigns @points=0" do
      expect(subject.instance_variable_get(:@points)).to eq(0)
    end
  end

  describe '#get_data' do
    it 'returns data array' do
      expect(subject.get_data).to eq(['testing-1', 'testing-2'])
    end
  end

  describe '#result' do
    let(:word) { 'testing' }

    context 'answer is wrong' do
      let(:answer) { 'tartes' }

      it 'returns wrong messages' do
        expect(subject.result(word, answer)).to eq('SALAH! Silakan coba lagi')
      end
    end

    context 'answer is correct' do
      let(:answer) { 'testing' }
      before { subject.instance_variable_set(:@points, 5) }

      it 'returns correct messages' do
        expect(subject.result(word, answer)).to eq('BENAR point anda : 6!')
      end
    end
  end

  describe '#shuffle' do
    it 'it return different string' do
      expect(subject.shuffle('testing')).not_to eq('testing')
    end

    it 'it return same total character' do
      expect(subject.shuffle('testing').size).to eq(7)
    end
  end
end
