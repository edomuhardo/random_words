class Main
  def initialize(data)
    @words = data
    @points = 0
  end

  def get_data
    @words
  end

  def result(word, answer)
    result = word == answer
    result ? "BENAR point anda : #{@points+=1}!" : 'SALAH! Silakan coba lagi'
  end

  def shuffle(word)
    # shuffle the characters of word
    word_array = word.split('')
    suffle_word = ''
    loop do
      suffle_word_array = word_array.shuffle
      suffle_word = suffle_word_array.join('')
      break if suffle_word != word
    end
    return suffle_word
  end
end
